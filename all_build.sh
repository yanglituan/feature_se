#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
[ -d release ] && rm -fr release
./build.sh debug
./build.sh release
popd
