:: Android NDK 交叉编译脚本(arm64-v8a)
:: author guyadong
:: 2020/12/21
@ECHO OFF
SETLOCAL

SET sh_folder=%~dp0

SET ANDROID_ABI=arm64-v8a

%sh_folder%ndk_build_mtfsdk.bat

ENDLOCAL