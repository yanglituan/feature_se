#!/bin/bash
# arm linux 交叉编译脚本,创建Makefile
# author guyadong
#字符串转大写
#参数：1字符串
function string_toupper()
{
    echo $1 | tr '[a-z]' '[A-Z]'
}
# 检测是否安装交叉编译器,没有安装则报错退出
[ -z "$(which arm-linux-gnueabihf-gcc)" ] && echo "ERROR:arm-linux-gnueabihf-gcc not install" && exit -1
[ -z "$(which arm-linux-gnueabihf-g++)" ] && echo "ERROR:arm-linux-gnueabihf-g++ not install" && exit -1

sh_folder=$(cd "$(dirname $0)"; pwd -P)
folder_name=$(basename $sh_folder) 
# 定义编译的版本类型(DEBUG|RELEASE)
build_type=RELEASE
OPT_COPY=1
OPT_BUILD=1
while [[ $# -gt 0 ]]
do
# 转大写
key=$(string_toupper $1)
case ${key} in
DEBUG)
	build_type=DEBUG;shift;;
NOCOPY)
	OPT_COPY=0;shift;;
NOBUILD)
	OPT_BUILD=0;shift;;
*) shift;;
esac
done

echo build_type=$build_type

prefix=$sh_folder/release/fse_mtfsdk_linux_arm71

pushd $sh_folder/..

[ -d $folder_name.armlinux.prj ] && rm -fr $folder_name.armlinux.prj
mkdir $folder_name.armlinux.prj

pushd $folder_name.armlinux.prj
# 编译基于MTFSDK linux(x86_64) 的 feature_se动态库
# EXT_SDK_TYPE 指定算法类型可选值： 
#                    CASSDK(默认值) 
#                    EUCLIDEAN  默认使用欧氏距离计算相似度 
#                    CUSTOM   使用自定义算法提的供相似度比较函数 
# 如果EXT_SDK_TYPE指定为EUCLIDEAN,下列参数需要设置:
# EUCLIDEAN_ELEM_TYPE 定义特征值数组类型(double/float)，如果不指定，默认值为double 
# EUCLIDEAN_ELEM_LEN  定义特征值数组长度 
# EUCLIDEAN_CODE_END_WITH_SUM  定义特征值数组最后是否有一个double保存特征值数组的点积和，默认为OFF 
# ============================下列为通用参数与EXT_SDK_TYPE无关
# FSE_LIBNAME        指定生成动态库名,不指定则使用默认值 
# JNI_FSE_LIBNAME    指定生成jni动态库名,不指定则使用默认值 
cmake "$sh_folder" -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=$build_type \
	-DJNI_FSE_LIBNAME=FS_FaceFeatureCompare \
	-DEXT_SDK_TYPE=EUCLIDEAN \
	-DEUCLIDEAN_ELEM_TYPE=float \
	-DEUCLIDEAN_ELEM_LEN=128 \
	-DEUCLIDEAN_CODE_END_WITH_SUM=OFF \
	-DCMAKE_INSTALL_PREFIX=$sh_folder/release/fse_mtfsdk_linux_arm71 \
	-DCMAKE_TOOLCHAIN_FILE=$sh_folder/cmake/toolchain-arm-linux-gnueabihf.cmake

dst_dir_prefix=$sh_folder/../mtfsdk
dst_dir=$dst_dir_prefix/mtfsdk-base/src/main/resources/linux-arm
if [ "$OPT_BUILD" = "1" ] ;
then
  cmake --build . --target install/strip -- -j8 || exit 
	if [ "$OPT_COPY" = "1" ] ;
	then
	    [ -d "$dst_dir_prefix" ] || exit 255
	    [ -d "$dst_dir" ] ||  mkdir -p "$dst_dir" || exit 
	    cp -fv "$prefix/lib/libFS_FaceFeatureCompare.so" "$dst_dir" || exit
	fi
fi

popd
popd

